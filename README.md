# 0000 - Mouse Motion Server

## Short Description

This is a very simple background server application that reads motion information of a connected USB mouse with a modified `xinput` program and serves the processed information though a websocket to a connected browser client. It is used specifically by the [Paranal/ALMA Webcam](https://gitlab.com/HITS_Supernova/0702_paranalalmawebcam) applications, which rotate the view to the panoramic image according to the orientation of the turnable screens (which is measured by a mounted optical mouse).

This application is used at the [ESO Supernova Planetarium and Visitor Centre](https://supernova.eso.org/?lang=en), Garching b. München.  
For more details about the project and how applications are run and managed within the exhibition please see [this link](https://gitlab.com/HITS_Supernova/overview).   

## Requirements / How-To

Hareware: a mouse needs to be connected to the PC (any connected mouse recognized by the X server can be used).

Software: you need Python 2.7 with the Twisted module installed (e.g. via `pip`).

## Detailed Information

An optical USB mouse is mounted at the upper end of a vertical pole and detects allows the connected PC to notice any rotation of the pole by motion events of the mouse. This is a very simple and cheap solution that avoids a more complex sensor or construction and is very simple to maintain or repair. For simplicity this application does not read the events directly from the X server, but simply runs a slightly modified version of `xinput --test-xi2`. The un-modified version already is able to print the relative mouse motion, but our small modifications filters only these events and print relative mouse motion in a simpler line-by-line format for dx and dy (e.g. `1.00 -2.00\n0.00 -1.00\n0.00 -3.00`). The python script `mouse-server.py` launches `xinput` in a separate thread, processes the data (adding up relative changes and scaling them according to a scaling parameter to an angle 0 to 360 degrees) and delivers it to a client though a websocket. Absolute mouse positions cannot be used for this since they will not get updated once the mouse pointer reaches the edge of the screen, while relative motion events will still be received.

All configuration like websocket port (default: 8889), scaling parameter and starting angle is handled through `cfg.py`. The scaling parameter is hardware-dependent and also be set with an environment variable `HILBERT_MOTION_SCALING` that is honored by `cfg.py`.

## Credits and License

Written by Volker Gaibler, HITS gGmbH. Original idea by Kai Polsterer, HITS gGmbH. This repository is licensed under the [MIT license](LICENSE).

Original `xinput` program by Philip Langdale, Frederic Lepied, Peter Hutterer, Red Hat Inc.
