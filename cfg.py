import os

# mouse motion input:
# starting angle and scaling factor for raw mouse values
# override scaling factor by setting environment variable
if "HILBERT_MOTION_SCALING" in os.environ:
    motion_scaling = float(os.environ["HILBERT_MOTION_SCALING"])
else:
    motion_scaling = -0.01764     # default value from 0702_1 exhibit tests

motion_starting_angle = 0.

# maximum values permitted for motion data
# introduced to ignore touch-induced data that mess it up
motion_data_maximum = 100.

# version number
version = 0.1

# server tcp port
ws_port = 8889

# server websocket url
ws_url = u"ws://localhost:{}/whatever".format(ws_port)


# verbose
verbose = 0

# xinput command line call
xinput_command = ["xinput-1.6.1/src/xinput", "--test-xi2", "--root"]

# generate sequential numbers for testing instead of reading actual input data?
dummy_operation = False
