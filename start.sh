#!/bin/sh

wd="$( dirname $(readlink -e $0) )"
cd "$wd"

# add "-v" to be verbose and print motion data"
exec venv-python.sh mouse-server.py

