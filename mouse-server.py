#!/usr/bin/python
# *-* coding: utf-8 *-*
#
# ===================
# Mouse Motion Server
# ===================


from twisted.internet import reactor, endpoints, task
from twisted.python import log    #  info: log.msg() and error: log.err()

from autobahn.twisted.websocket import WebSocketServerProtocol, \
    WebSocketServerFactory

import json
import math
import psutil
import Queue
import sys
import threading
import time
import traceback
from subprocess import PIPE

# configuration variables
import cfg


###############################################################################
class WsServerFactory(WebSocketServerFactory):

    def startFactory(self):
        """Preparations."""
        self.conns = []
        self.last_msg = str(cfg.motion_starting_angle)   # start value

    def register_connection(self, protocol):
        self.conns.append(protocol)

    def unregister_connection(self, protocol):
        self.conns.remove(protocol)

    def send_to_all(self, msg):
        #log.msg("factory send_to_all:")
        self.last_msg = msg
        for conn in self.conns:
            conn.sendMessage(msg)

    def close_all(self):
        log.msg("Closing all connections...")
        for conn in self.conns:
            #conn.transport.loseConnection()
            conn.sendClose()


###############################################################################
class WsServerProtocol(WebSocketServerProtocol):

    def onConnect(self, request):
        self.peer = request.peer
        log.msg("New client connecting: {0}".format(request.peer))

    def onOpen(self):
        log.msg("WebSocket connection open.")
        self.factory.register_connection(self)
        #msg = "Hello client [{}]. Serving screen rotation angle to you, range [0...360] degrees.".format(self.peer)
        # re-send last motion message if there is one
        if self.factory.last_msg:
            msg = self.factory.last_msg
            self.sendMessage(msg, isBinary=False)

    def onMessage(self, payload, isBinary):
        if isBinary:
            log.msg("Binary message received: {0} bytes".format(len(payload)))
        else:
            log.msg("Text message received: {0}".format(payload.decode('utf8')))

        # echo back message verbatim
        self.sendMessage(payload, isBinary)

    def onClose(self, wasClean, code, reason):
        log.msg("WebSocket connection closed: {0}".format(reason))
        self.factory.unregister_connection(self)


###############################################################################
class MouseMonitor(object):

    def __init__(self):

        self.checks_per_second = 30.0
        self.process = None
        self.xtot = 0.
        self.ytot = 0.
        self.factory = None


    def run(self):
        """Starts everything up ("main")."""

        log.startLogging(sys.stdout) 
        log.msg("========== Mouse Motion Monitor Server ==========")
        log.msg("verbose = {}".format(cfg.verbose))

        #self.start_mouse_monitor()
        self.prepare_websocket_server()

        # start mouse monitor, which runs in a producer thread
        self.start_mouse_monitor()

        # Set up a looping call regularly to check delivered mouse events
        #self.lc = task.LoopingCall(self.check_mouse)
        #self.lc.start(1. / self.checks_per_second, now=False)

        lc = task.LoopingCall(self.regular_notifcation)
        lc.start(10., now=False)

        # start reactor
        reactor.run()


    def regular_notifcation(self):
        """Just for testing."""
        pass
        #self.factory.send_to_all("Hello again. It's now {0}.".format(time.asctime()))


    def prepare_websocket_server(self):
        """Prepares server to get then launched by the reactor (once it takes control)."""

        log.msg("Server URL: {0}".format(cfg.ws_url))
        self.factory = WsServerFactory(cfg.ws_url)
        self.factory.protocol = WsServerProtocol
        # factory.setProtocolOptions(maxConnections=2)

        ep = endpoints.serverFromString(reactor, "tcp:{}".format(cfg.ws_port))
        ep.listen(self.factory)

        # close websockets right before reactor shutdown happens
        reactor.addSystemEventTrigger("before", "shutdown", self.factory.close_all)


    def start_mouse_monitor(self):

        # shared "shutdown" event
        self.t_stop_event = threading.Event()

        # create a shared queue for the threads data communication(put / get)
        self.t_queue = Queue.Queue()
        self.t_producer = threading.Thread(target=self.mouse_monitor_thread, args=(self.t_queue,))
        self.t_producer.daemon = True  # everything exits with the main program
        self.t_producer.start()


    def mouse_monitor_thread(self, q):
        """Starts xinput for mouse motion detection and handles its stdout.
        q is a Queue that allows safe message passing between the threads: producer put, consumer get.
        """

        process = None
        log.msg("Producer: starting xinput...")

        angle = cfg.motion_starting_angle
        xtot = 0.
        ytot = 0.
        while 1:

            if cfg.dummy_operation:
                #
                # dummy data:
                # values around 0 degree to check smooth transition between adjacent images
                #
                if cfg.verbose > 0:
                    log.msg("output: dummy angle={}".format(angle))
                msg = str(angle % 360.)
                self.publish_mouse(msg)
                time.sleep(0.05)
                angle += 0.01
                if angle > 0.2:
                    angle = -angle

            else:
                #
                # data read by "xinput" call (patched xinput binary!)
                # tests delivered up to ~130 reads (lines) per second
                #
                if process is None:
                    process = psutil.Popen(cfg.xinput_command, stdout=PIPE)
                output = process.stdout.readline()
                if output:
                    dx, dy = map(float, output.split())

                    # ignore motion data that are way too large
                    # (to prevent invalid data messing up the motion)
                    if abs(dx) > cfg.motion_data_maximum or abs(dy) > cfg.motion_data_maximum:
                        continue

                    angle += cfg.motion_scaling * (dx + dy)
                    angle %= 360
                    xtot += dx
                    ytot += dy
                    if cfg.verbose > 0:
                        log.msg("output: dx={} dy={}  xtot={} ytot={} angle={}".format(dx, dy, xtot, ytot, angle))
                    msg = str(angle)

                    # publish via websockets (a tasks for the reactor)
                    self.publish_mouse(msg)
                else:
                    # process died, return value see process.poll() 
                    break


        # child process died, nothing left to do
        log.msg("Producer is done (xinput is dead, return value {}).".format(process.poll()))
        reactor.callLater(1., self.shutdown)
        #reactor.stop()
        #self.t_stop_event.set()  # not used currently


    def shutdown(self):
        log.msg("Shutting down...")
        #self.factory.close_all()
        if reactor.running:
            reactor.stop()
        #self.stop_mouse_monitor()


    def publish_mouse(self, msg):
        self.factory.send_to_all(msg)
        if cfg.verbose > 1:
            log.msg("publish_mouse: " + msg)



###############################################################################
if __name__ == "__main__":

    if len(sys.argv) > 1 and sys.argv[1] == "-v":
        cfg.verbose = 1

    log.msg("motion_scaling: {}, motion_starting_angle: {}".format(
        cfg.motion_scaling, cfg.motion_starting_angle))

    m = MouseMonitor()
    m.run()
    log.msg("Done.")


